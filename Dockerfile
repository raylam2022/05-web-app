FROM openjdk:8-jre-alpine
COPY target/web-app-1.0-SNAPSHOT-jar-with-dependencies.jar /
USER: 1000:1000
CMD ["java", "-jar", "/web-app-1.0-SNAPSHOT-jar-with-dependencies.jar"]
